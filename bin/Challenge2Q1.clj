(ns Challenge2Q1
  (:use clojure.test)
  (:use clojure.string)
)

(defn stringMap [myString] 
  (frequencies myString)
)

(defn anagram? [string1 string2]
  (= (stringMap (lower-case string1)) (stringMap (lower-case string2))))

(deftest god-dog
  (is
    (= true (anagram? "god" "dog")))
  (is
    (= true (anagram? "God" "dog")))
  (is
    (= true (anagram? "goody" "doogy")))
  (is
    (= false (anagram? "abc" "xyz")))
  (is
    (= true (anagram? "I am a God" "A dog am I")))
)

(run-tests)