(ns Challenge2Q2
  (:use clojure.test)
  (:use clojure.string)
)

(defn htmlSpace [string]
  (replace string " " "%20")
  )

(deftest replaceSpace
  (is
    (= "ben%20schleis" (htmlSpace "ben schleis"))
    )
)

(run-tests)