(ns Challenge5
  (:use clojure.test)
  (:use clojure.string)
)
(import '[java.util Date])
(import '[java.text SimpleDateFormat]) 

  (defn stringConvert [line]
  (replace (replace line "~" "") "^" "\t" )
 )
  
(defn convertFile [readFile writeFile]
  (spit writeFile (stringConvert (slurp readFile)))
  )

(defn timestamp
  ([] (str (. (new Date) getTime)))
  ([dateString]
                  (def date (. (new SimpleDateFormat "mm-dd-yyyy") parse dateString))
                  (str (. date getTime))
            )
)

(deftest ConvertString
  (is
    (= "some\tthing\twicked" (stringConvert "~some~^~thing~^~wicked~"))
    )
  (is
    (= "\t\t\t" (stringConvert "^^^"))
    )
 )

(deftest fileConvert
  (spit "testFile" "~some~^~thing~^~wicked~\n~to~^~be~^~or~^~not~")
  (convertFile "testFile" "newFile")
  (is
      (= "some\tthing\twicked\nto\tbe\tor\tnot" (slurp "newFile"))
  )
)

(deftest getTimestamp
  (is
    (= "18060000" (timestamp "1-1-1970"))
    (= (str (. (new Date) getTime)) (timestamp))
   )
 )

(run-tests)

(convertFile "DATA_SRC.txt" (timestamp))
