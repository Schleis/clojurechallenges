(ns Challenge4
  (:use clojure.test)
  )

(defn smallestCommonNumber
  ([list1] (first list1))
  ([list1 list2] (first (intersection (set list1) (set list2))))
  ([list1 list2 & more] (first (intersection list1 list2 more)))
 )
  

(deftest test1
  (is
    (= 3 (smallestCommonNumber [3 4 5]))))
 
(deftest test2
  (is
    (= 4 (smallestCommonNumber [1 2 3 4 5 6 7] [0.5 3/2 4 19]))))
 
(deftest test3
  (is
    (= 7 (smallestCommonNumber (range) (range 0 100 7/6) [2 3 5 7 11 13]))))
 
(deftest test4
  (is
    (= 64 (smallestCommonNumber (map #(* % % %) (range)) ;; perfect cubes
          (filter #(zero? (bit-and % (dec %))) (range)) ;; powers of 2
          (iterate inc 20))))) ;; at least as large as 20

(run-tests)