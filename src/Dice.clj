(ns Dice
  (:use clojure.test)
)

(defn fact [n] 
  ;Find the factorial (!) of a number n
  (reduce * (range 1 (+ n 1)))
)
(defn PI 
  ;The product of the numbers within the range
  [n1 n2] (reduce * (range n1 (+ 1 n2)))
)

(deftest testFactorial
  (is
    (= 120 (fact 5))
  )
  (is
    (= 1 (fact 1))
  )
  (is
    (= 2 (fact 2))
  )
)

(deftest testPI
  (is
    (= 720 (PI 2 6))
  )
  (is
    (= 120 (PI 1 5))
  )
  (is
    (= 1320 (PI 10 (+ 10 2)))
  )
)

(defn numberOfCombinationsWithRepitition 
  ;;Determine the total number of combinations allowing for repitition
  ;;n is the number of options to choose from
  ;;k is the number of items chosen
  [n k] (/ (PI n (+ n (- k 1))) (fact k)))

(deftest testCombinations
  (is
    (= 220 (numberOfCombinationsWithRepitition 10 3))
  )
  (is
    (= 455 (numberOfCombinationsWithRepitition 4 12))
  )
  (is
    (= 21 (numberOfCombinationsWithRepitition 6 2))
  )
)

(defn probabilityOfHalfDiceSame 
  ;;Find the probability of the half of a given number of dice being the same value
  ;;sides is the number of faces on the dice
  ;;numDice is the number of Dice
  ;;TODO: Add error checking to allow only even numbers of dice
  [sides numDice] (/ (numberOfCombinationsWithRepitition (- sides 1) (/ numDice 2)) (numberOfCombinationsWithRepitition sides numDice)))

(deftest testProbability
  (is
    (= (/ 5 21) (probabilityOfHalfDiceSame 6 2))
  )
  (is
    (= (/ 15 126) (probabilityOfHalfDiceSame 6 4))
  )
)

(run-tests)
