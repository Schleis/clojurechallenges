(ns ClojureChallenge
  (:use clojure.test)
)

(defn nilKey? [k L] 
  (prn (contains? L k))
  (and
    (contains? L k)
    (nil? (L k))
  )
 )

(deftest checks
  (is
    (= true (nilKey? :a {:a nil :b 2}))
   )
  (is
    (= false (nilKey? :b {:a nil :b 2}))
  )
  (is
    (= false (nilKey? :c {:a nil :b 2}))
   )
)

(run-tests)