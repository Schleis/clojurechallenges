(ns vectorOperations
  (:use clojure.test)
  )

(defn dot [vect1 vect2]
    (if (= (count vect1) (count vect2))
      ((for [x range((count vect1))] [x])
        (* (nth vect1 x) (nth vect2 x)))
      (println "Vectors are not the same dimension")
     )
)

(dot '(1 3 -5) '(4 -2 -1))